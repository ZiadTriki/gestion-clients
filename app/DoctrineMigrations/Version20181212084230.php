<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181212084230 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tb_devis (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, note VARCHAR(255) DEFAULT NULL, url_logo VARCHAR(255) DEFAULT NULL, curency VARCHAR(255) DEFAULT NULL, code_devis VARCHAR(255)  , montant_devis DOUBLE PRECISION  , list_services LONGTEXT   COMMENT \'(DC2Type:json_array)\', taxe LONGTEXT   COMMENT \'(DC2Type:json_array)\', total DOUBLE PRECISION  , etat TINYINT(1)  , status TINYINT(1) DEFAULT NULL, avis_client TINYINT(1)  , date_creation DATE  , INDEX IDX_E1CC95DB19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tb_facture (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, note VARCHAR(255)  , status TINYINT(1) DEFAULT NULL, url_logo VARCHAR(255)  , curency VARCHAR(255)  , code_facture VARCHAR(255)  , montant_facture DOUBLE PRECISION  , list_services LONGTEXT   COMMENT \'(DC2Type:json_array)\', taxe LONGTEXT   COMMENT \'(DC2Type:json_array)\', total DOUBLE PRECISION  , etat TINYINT(1)  , etat_paiment VARCHAR(255)  , date_creation DATE  , date_de_paiment DATE  , INDEX IDX_8151BEB219EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tb_paiement (id INT AUTO_INCREMENT NOT NULL, facture_id INT DEFAULT NULL, montantTotale DOUBLE PRECISION  , montantPayer DOUBLE PRECISION  , montantRest DOUBLE PRECISION DEFAULT NULL, methodePaiment VARCHAR(255) DEFAULT NULL, status TINYINT(1) DEFAULT NULL, INDEX IDX_897B6F007F2DEE08 (facture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tb_services (id INT AUTO_INCREMENT NOT NULL, libele VARCHAR(255)  , description LONGTEXT  , prix DOUBLE PRECISION  , status TINYINT(1)  , PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tb_tax (id INT AUTO_INCREMENT NOT NULL, nomTax VARCHAR(255)  , typeTax VARCHAR(255)  , isDiscount TINYINT(1) DEFAULT NULL, valeurTax DOUBLE PRECISION DEFAULT NULL, status TINYINT(1)  , PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255)  , last_name VARCHAR(255)  , email VARCHAR(255)  , password VARCHAR(255)  , type_currency VARCHAR(255)  , company_name VARCHAR(255)  , company_address VARCHAR(255)  , company_phone VARCHAR(255)  , company_email INT  , country VARCHAR(255)  , status TINYINT(1)  , roles LONGTEXT   COMMENT \'(DC2Type:json_array)\', url_image VARCHAR(255)  , UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tb_devis ADD CONSTRAINT FK_E1CC95DB19EB6921 FOREIGN KEY (client_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tb_facture ADD CONSTRAINT FK_8151BEB219EB6921 FOREIGN KEY (client_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tb_paiement ADD CONSTRAINT FK_897B6F007F2DEE08 FOREIGN KEY (facture_id) REFERENCES tb_facture (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tb_paiement DROP FOREIGN KEY FK_897B6F007F2DEE08');
        $this->addSql('ALTER TABLE tb_devis DROP FOREIGN KEY FK_E1CC95DB19EB6921');
        $this->addSql('ALTER TABLE tb_facture DROP FOREIGN KEY FK_8151BEB219EB6921');
        $this->addSql('DROP TABLE tb_devis');
        $this->addSql('DROP TABLE tb_facture');
        $this->addSql('DROP TABLE tb_paiement');
        $this->addSql('DROP TABLE tb_services');
        $this->addSql('DROP TABLE tb_tax');
        $this->addSql('DROP TABLE user');
    }
}
