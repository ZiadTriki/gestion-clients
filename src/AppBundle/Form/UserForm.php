<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName')->
        add('lastName')->
        add('email')->
        add('plainPassword',PasswordType::class)->
        add('typeCurrency', CurrencyType::class)->
        add('companyName')->
        add('companyAddress')->
        add('companyPhone')->
        add('companyEmail',EmailType::class)->
        add('country',CountryType::class)->
        add('roles' ,ChoiceType::class,array(
            'choices'=>array(
                'Administrateur'=>'ROLE_ADMIN',
                'Client'=>'ROLE_CLIENT'
            ),
            'preferred_choices'=>'ROLE_ADMIN'
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\User']);

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_user';
    }
}
