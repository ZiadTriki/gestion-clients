<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username',TextType::class, [
                'label_attr'=>[
                    'class'=>'sr-only'
                ],
                'attr'=>[
                    'placeholder'=>'username'
                ]
            ])
            ->add('_password',PasswordType::class,[
                'label_attr'=>[
                    'class'=>'sr-only'
                ],
                'attr'=>[
                    'placeholder'=>'password'
                ]
            ]);
          /*  ->add('_remember_me',CheckboxType::class,[
                'label'=>'Remember Me',
                'required'=>false,

            ]);*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
       //$resolver->setDefault('User');
    }

    public function getName()
    {
        return 'app_bundle_login_form';
    }
}
