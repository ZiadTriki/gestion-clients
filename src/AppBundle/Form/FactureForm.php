<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('Client',EntityType::class,array('class'=>User::class,'query_builder'=>function(UserRepository $er){

            return $er->retirerUserByOrdreAlpha();})

        )->add('codeFacture')->add('')->add()->add()->add()->add('montantDevis')->add('montantRemise');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

            'data_class' => 'AppBundle\Entity\Facture'
        ]);

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_facture_form';
    }
}
