<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaxeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('nomTax')->
         add('typeTax',ChoiceType::class,array('choices'=>
         array('Valeur'=>'valeur','Pourcentage'=>'pourcentage')))->
         add('isDiscount')->
         add('valeurTax');


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
           'data_class'=>'AppBundle\Entity\Tax'
        ]);

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_taxe_form';
    }
}
