<?php


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User implements UserInterface
{



    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string")
     */
    private $firstName;
    /**
     * @ORM\Column(type="string")
     */
    private $lastName;
    /**
     * @ORM\Column(type="string",unique=true)
     */
    private $email;
    /**
     * @ORM\Column(type="string")
     */
    private $password;
    /**
     * @ORM\Column(type="string")
     */
    private $typeCurrency;
    /**
     * @ORM\Column(type="string")
     */
    private $companyName;
    /**
     * @ORM\Column(type="string")
     *
     */
    private $companyAddress;
    /**
     * @ORM\Column(type="string")
     *
     *
     */
    private $companyPhone;
    /**
     * @ORM\Column(type="integer")
     *
     */
    private $companyEmail;
    /**
     * @ORM\Column(type="string")
     *
     */
    private $country;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = [];



    private $plainPassword;

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param  $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        $this->password=null;
    }


    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @ORM\Column(type="string")
     * @Assert\IsNull
     *
     */
    private $urlImage;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Facture",mappedBy="Client")
     */
    private $clientFacture;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Devis",mappedBy="Client")
     */

    private $clientDevis;
    /**
     * @return mixed
     */
    public function getUrlImage()
    {
        return $this->urlImage;
    }

    /**
     * @param mixed $urlImage
     */
    public function setUrlImage($urlImage)
    {
        $this->urlImage = $urlImage;
    }

    /**
     * @return String
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTypeCurrency()
    {
        return $this->typeCurrency;
    }

    /**
     * @param mixed $typeCurrency
     */
    public function setTypeCurrency($typeCurrency)
    {
        $this->typeCurrency = $typeCurrency;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * @param mixed $companyAddress
     */
    public function setCompanyAddress($companyAddress)
    {
        $this->companyAddress = $companyAddress;
    }

    /**
     * @return mixed
     */
    public function getCompanyPhone()
    {
        return $this->companyPhone;
    }

    /**
     * @param mixed $companyPhone
     */
    public function setCompanyPhone($companyPhone)
    {
        $this->companyPhone = $companyPhone;
    }

    /**
     * @return mixed
     */
    public function getCompanyEmail()
    {
        return $this->companyEmail;
    }

    /**
     * @param mixed $companyEmail
     */
    public function setCompanyEmail($companyEmail)
    {
        $this->companyEmail = $companyEmail;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }


    public function __construct()
    {
        $this->clientFacture=new ArrayCollection();
        $this->clientDevis=new ArrayCollection();

    }

    /**
     * @return ArrayCollection|Facture[]
     */
    public function getClientFacture()
    {
        return $this->clientFacture;
    }

    /**
     * @return ArrayCollection|Devis[]
     */
    public function getClientDevis()
    {
        return $this->clientDevis;
    }


    public function getRoles()
    {


        return array($this->roles);

    }
    public function setRoles($roles)
    {
        return $this->roles=$roles;

    }
    public function getSalt()
    {
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        $this->plainPassword=null;
    }

    public function __toString()
    {
        return $this->getFirstName();
    }


}