<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tax
 *
 * @ORM\Table(name="tb_tax")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaxRepository")
 */
class Tax
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomTax", type="string", length=255)
     */
    private $nomTax;

    /**
     * @var string
     *
     * @ORM\Column(name="typeTax", type="string", length=255)
     */
    private $typeTax;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDiscount", type="boolean", nullable=true)
     */
    private $isDiscount;

    /**
     * @var float
     *
     * @ORM\Column(name="valeurTax", type="float", nullable=true)
     */
    private $valeurTax;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomTax
     *
     * @param string $nomTax
     *
     * @return Tax
     */
    public function setNomTax($nomTax)
    {
        $this->nomTax = $nomTax;

        return $this;
    }

    /**
     * Get nomTax
     *
     * @return string
     */
    public function getNomTax()
    {
        return $this->nomTax;
    }

    /**
     * Set typeTax
     *
     * @param string $typeTax
     *
     * @return Tax
     */
    public function setTypeTax($typeTax)
    {
        $this->typeTax = $typeTax;

        return $this;
    }

    /**
     * Get typeTax
     *
     * @return string
     */
    public function getTypeTax()
    {
        return $this->typeTax;
    }

    /**
     * Set isDiscount
     *
     * @param boolean $isDiscount
     *
     * @return Tax
     */
    public function setIsDiscount($isDiscount)
    {
        $this->isDiscount = $isDiscount;

        return $this;
    }

    /**
     * Get isDiscount
     *
     * @return bool
     */
    public function getIsDiscount()
    {
        return $this->isDiscount;
    }

    /**
     * Set valeurTax
     *
     * @param float $valeurTax
     *
     * @return Tax
     */
    public function setValeurTax($valeurTax)
    {
        $this->valeurTax = $valeurTax;

        return $this;
    }

    /**
     * Get valeurTax
     *
     * @return float
     */
    public function getValeurTax()
    {
        return $this->valeurTax;
    }
}

