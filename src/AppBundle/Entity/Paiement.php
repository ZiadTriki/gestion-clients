<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 *
 * @ORM\Table(name="tb_paiement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaimentRepository")
 */
class Paiement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Facture",inversedBy="paiement")
     */
    private $facture;

    /**
     * @return mixed
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * @param mixed $facture
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;
    }
    /**
     * @var float
     *
     * @ORM\Column(name="montantTotale", type="float")
     */
    private $montantTotale;

    /**
     * @var float
     *
     * @ORM\Column(name="montantPayer", type="float")
     */
    private $montantPayer;

    /**
     * @var float
     *
     * @ORM\Column(name="montantRest", type="float", nullable=true)
     */
    private $montantRest;

    /**
     * @var string
     *
     * @ORM\Column(name="methodePaiment", type="string", length=255, nullable=true)
     */
    private $methodePaiment;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montantTotale
     *
     * @param float $montantTotale
     *
     * @return Paiement
     */
    public function setMontantTotale($montantTotale)
    {
        $this->montantTotale = $montantTotale;

        return $this;
    }

    /**
     * Get montantTotale
     *
     * @return float
     */
    public function getMontantTotale()
    {
        return $this->montantTotale;
    }

    /**
     * Set montantPayer
     *
     * @param float $montantPayer
     *
     * @return Paiement
     */
    public function setMontantPayer($montantPayer)
    {
        $this->montantPayer = $montantPayer;

        return $this;
    }

    /**
     * Get montantPayer
     *
     * @return float
     */
    public function getMontantPayer()
    {
        return $this->montantPayer;
    }

    /**
     * Set montantRest
     *
     * @param float $montantRest
     *
     * @return Paiement
     */
    public function setMontantRest($montantRest)
    {
        $this->montantRest = $montantRest;

        return $this;
    }

    /**
     * Get montantRest
     *
     * @return float
     */
    public function getMontantRest()
    {
        return $this->montantRest;
    }

    /**
     * Set methodePaiment
     *
     * @param string $methodePaiment
     *
     * @return Paiement
     */
    public function setMethodePaiment($methodePaiment)
    {
        $this->methodePaiment = $methodePaiment;

        return $this;
    }

    /**
     * Get methodePaiment
     *
     * @return string
     */
    public function getMethodePaiment()
    {
        return $this->methodePaiment;
    }
}

