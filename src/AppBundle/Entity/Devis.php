<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DevisRepository")
 * @ORM\Table(name="tb_devis")
 */
class Devis
{


    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $note;
    /**
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $urlLogo;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $curency;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="clientDevis")
     *
     */
    private $Client;
    /**
     * @ORM\Column(type="string")
     */
    private $codeDevis;
    /**
     * @ORM\Column(type="float")
     */
    private $montantDevis;

    /**
     * @ORM\Column(type="json_array")
     */
    private $listServices;
    /**
     * @ORM\Column(type="json_array")
     */
    private $taxe;
    /**
     * @ORM\Column(type="float")
     */
    private $total;
    /**
     * @ORM\Column(type="boolean")
     */
    private $etat;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $status;
    /**
     * @ORM\Column(type="boolean")
     */
    private $avisClient;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    /**
     * @return mixed
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * @param mixed $idClient
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;
    }

    /**
     * @return mixed
     */
    public function getCodeDevis()
    {
        return $this->codeDevis;
    }

    /**
     * @param mixed $codeDevis
     */
    public function setCodeDevis($codeDevis)
    {
        $this->codeDevis = $codeDevis;
    }

    /**
     * @return mixed
     */
    public function getMontantDevis()
    {
        return $this->montantDevis;
    }

    /**
     * @param mixed $montantDevis
     */
    public function setMontantDevis($montantDevis)
    {
        $this->montantDevis = $montantDevis;
    }

    /**
     * @return mixed
     */
    public function getMontantRemise()
    {
        return $this->montantRemise;
    }

    /**
     * @param mixed $montantRemise
     */
    public function setMontantRemise($montantRemise)
    {
        $this->montantRemise = $montantRemise;
    }

    /**
     * @return mixed
     */
    public function getListServices()
    {
        return $this->listServices;
    }

    /**
     * @param mixed $listServices
     */
    public function setListServices($listServices)
    {
        $this->listServices = $listServices;
    }

    /**
     * @return mixed
     */
    public function getTaxe()
    {
        return $this->taxe;
    }

    /**
     * @param mixed $taxe
     */
    public function setTaxe($taxe)
    {
        $this->taxe = $taxe;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getAvisClient()
    {
        return $this->avisClient;
    }

    /**
     * @param mixed $avisClient
     */
    public function setAvisClient($avisClient)
    {
        $this->avisClient = $avisClient;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }


    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->Client;
    }

    /**
     * @param mixed $Client
     */
    public function setClient(User $Client)
    {
        $this->Client = $Client;
    }


    /**
     * @return mixed
     */
    public function getUrlLogo()
    {
        return $this->urlLogo;
    }

    /**
     * @param mixed $urlLogo
     */
    public function setUrlLogo($urlLogo)
    {
        $this->urlLogo = $urlLogo;
    }

    /**
     * @return mixed
     */
    public function getCurency()
    {
        return $this->curency;
    }

    /**
     * @param mixed $curency
     */
    public function setCurency($curency)
    {
        $this->curency = $curency;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

}