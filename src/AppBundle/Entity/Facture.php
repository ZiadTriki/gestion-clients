<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FactureRepository")
 * @ORM\Table(name="tb_facture")
 */
class Facture
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $note;
    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function getPaiement()
    {
        return $this->paiement;
    }

    /**
     * @param mixed $paiement
     */
    public function setPaiement($paiement)
    {
        $this->paiement = $paiement;
    }

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Paiement",mappedBy="facture")
     */
    private $paiement;
    /**
     *
     * @ORM\Column(type="string")
     */
    private $urlLogo;
    /**
     * @ORM\Column(type="string")
     */
    private $curency;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",inversedBy="clientFacture")
     *
     */
    private $Client;
    /**
     * @ORM\Column(type="string")
     */
    private $codeFacture;
    /**
     * @ORM\Column(type="float")
     */
    private $montantFacture;

    /**
     * @ORM\Column(type="json_array")
     */
    private $listServices;
    /**
     * @ORM\Column(type="json_array")
     */
    private $taxe;
    /**
     * @ORM\Column(type="float")
     */
    private $total;
    /**
     * @ORM\Column(type="boolean")
     */
    private $etat;
    /**
     * @ORM\Column(type="string")
     */
    private $etatPaiment;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDePaiment;

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getUrlLogo()
    {
        return $this->urlLogo;
    }

    /**
     * @param mixed $urlLogo
     */
    public function setUrlLogo($urlLogo)
    {
        $this->urlLogo = $urlLogo;
    }

    /**
     * @return mixed
     */
    public function getCurency()
    {
        return $this->curency;
    }

    /**
     * @param mixed $curency
     */
    public function setCurency($curency)
    {
        $this->curency = $curency;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->Client;
    }

    /**
     * @param mixed $Client
     */
    public function setClient($Client)
    {
        $this->Client = $Client;
    }

    /**
     * @return mixed
     */
    public function getCodeFacture()
    {
        return $this->codeFacture;
    }

    /**
     * @param mixed $codeFacture
     */
    public function setCodeFacture($codeFacture)
    {
        $this->codeFacture = $codeFacture;
    }

    /**
     * @return mixed
     */
    public function getMontantFacture()
    {
        return $this->montantFacture;
    }

    /**
     * @param mixed $montantFacture
     */
    public function setMontantFacture($montantFacture)
    {
        $this->montantFacture = $montantFacture;
    }

    /**
     * @return mixed
     */
    public function getListServices()
    {
        return $this->listServices;
    }

    /**
     * @param mixed $listServices
     */
    public function setListServices($listServices)
    {
        $this->listServices = $listServices;
    }

    /**
     * @return mixed
     */
    public function getTaxe()
    {
        return $this->taxe;
    }

    /**
     * @param mixed $taxe
     */
    public function setTaxe($taxe)
    {
        $this->taxe = $taxe;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getEtatPaiment()
    {
        return $this->etatPaiment;
    }

    /**
     * @param mixed $etatPaiment
     */
    public function setEtatPaiment($etatPaiment)
    {
        $this->etatPaiment = $etatPaiment;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return mixed
     */
    public function getDateDePaiment()
    {
        return $this->dateDePaiment;
    }

    /**
     * @param mixed $dateDePaiment
     */
    public function setDateDePaiment($dateDePaiment)
    {
        $this->dateDePaiment = $dateDePaiment;
    }









}