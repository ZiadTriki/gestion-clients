<?php

namespace AppBundle\Doctrine;


use AppBundle\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HashPasswordListener implements EventSubscriber
{
    private $passwordEncoder;

    /**
     * @param UserPasswordEncoder $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    
    public function getSubscribedEvents()
    {
        return ['prePersist'];
    }

    public function prePersist(LifecycleEventArgs $args){
        $entity = $args->getEntity();
        if(! $entity instanceof User){
            return null;
        }

        $this->setEncodePassword($entity);

    }

    public function preUpdate(PreUpdateEventArgs $eventArgs) {
        $entity = $eventArgs->getEntity();
        if(! $entity instanceof User){
            return null;
        }
        $this->setEncodePassword($entity);
        $em = $eventArgs->getEntityManager();
        $metaData = $em->getClassMetadata(get_class($entity));

        $em->getUnitOfWork()->recomputeSingleEntityChangeSet($metaData,$entity);
    }

    /**
     * @param User $user
     */
    private function setEncodePassword(User $user){
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,$user->getPlainPassword()
            )
        );

    }


}