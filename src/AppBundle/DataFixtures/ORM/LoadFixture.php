<?php


namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;
use Nelmio\Alice\Fixtures\Fixture;

class LoadFixture implements ORMFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        Fixtures::load(__DIR__ . '/fixtures.yml', $manager, ['providers' => [$this]]);


    }

    public function service()
    {
        $tab = ['Creation Site Web', 'Creation Logo', 'Creation Flyer'];
        $key = array_rand($tab);
        return $tab[$key];

    }

    public function description()
    {
        $tab = ['Creation Site Web', 'Creation Logo', 'Creation Flyer'];
        $key = array_rand($tab);
        return $tab[$key];

    }
    public function money()
    {
        $tab = ['Dollar', 'Euro', 'Dinar'];
        $key = array_rand($tab);
        return $tab[$key];

    }
    public function methodePaiment()
    {
        $tab = ['cash', 'bank'];
        $key = array_rand($tab);
        return $tab[$key];

    }



}