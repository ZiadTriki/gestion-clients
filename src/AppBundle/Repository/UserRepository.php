<?php


namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function retirerUserByOrdreAlpha()
    {
        return $this->createQueryBuilder('user')->orderBy('user.firstName','ASC');
    }
    public function retirerToutClient()
    {

        return $this->createQueryBuilder('user')->select('user.roles','user.firstName','user.lastName','user.email') ->where('user.roles LIKE :roles')
            ->setParameter('roles', '%ROLE_CLIENT%')->getQuery()->execute();
    }
    public function retirerToutAdminstrateur()
    {
        return $this->createQueryBuilder('user')->select('user.firstName','user.lastName','user.email','user.roles') ->where('user.roles LIKE :roles')
            ->setParameter('roles', '%ROLE_ADMIN%')->getQuery()->execute();    }



}