<?php


namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;

class FactureRepository extends EntityRepository
{

    public function retirerToutFactureAdministrateur(){

           return $this->findAll();

    }
    public function retirerToutFactureClient($idClient){

        return $this->createQueryBuilder('facture')
            ->andWhere("facture.Client = :id")
            ->setParameter("id", $idClient )
            ->getQuery()
            ->execute();

    }


}