<?php


namespace AppBundle\Security;
use AppBundle\Form\LoginForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class LoginFormAuth extends AbstractFormLoginAuthenticator
{
    private $formFactory;

    private $em;

    private $router;

    private $passwordEncode;

    private $request;
    /**
     * LoginFormAuthenticator constructor.
     * @param $form/FormFactoryInterface
     */
    public function __construct(FormFactoryInterface $form,
                                EntityManagerInterface $em, RouterInterface $router,
                                UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->formFactory = $form;
        $this->em = $em;
        $this->router = $router;
        $this->passwordEncode = $passwordEncoder;
    }

    public function getCredentials(Request $request)
    {
        $isSubmit = $request->getPathInfo() == '/' &&
            $request->isMethod('POST');
        if(!$isSubmit) {
            return null;
        }
        $form = $this->formFactory->create(LoginForm::class);
        $form->handleRequest($request);

        $data = $form->getData();
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['_username']
        );


        return $data;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['_username'];

        return $this->em->getRepository('AppBundle:User')
            ->findOneBy(['email'=>$username]);

    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['_password'];

        if(!$this->passwordEncode->isPasswordValid($user,$password)){
            return false;
        }
        return true;
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('login_route');
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {


        return new RedirectResponse($this->getLoginUrl());
    }


    protected function getDefaultSuccessRedirectUrl(){
        return $this->router->generate('dashboard_index');
    }
}