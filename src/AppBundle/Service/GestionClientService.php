<?php


namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class GestionClientService
{


    private $serializer;
    private $em;

    public function __construct(SerializerInterface $serializer,EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->em=$em;
    }

    /**
     * @param $obj
     * @return []
     */
    public function transformObjectToArray($obj)

    {

        return json_decode($this->serializer->serialize($obj, 'json'));


    }

    /**
     * @param $obj
     * @return []
     */
    public function transormeObjectToJson($obj)
    {
        return $this->serializer->serialize($obj, 'json');
    }

    /**
     * @param
     */
    public function inserFormToDataBase($obj)
    {

        $this->em->persist($obj);
        $this->em->flush();



    }



}