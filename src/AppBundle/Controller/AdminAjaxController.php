<?php


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminAjaxController extends Controller
{

    /**
     * @Route("service/getService",name="get_service_ajax")}
     */
    public function getServiceAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutService = $em->getRepository('AppBundle:Service')->findAll();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutService)];
        return new JsonResponse($response);


    }

    /**
     * @Route("factures/getFacture",name="get_facture_ajax")}
     */
    public function getFactureAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutService = $em->getRepository('AppBundle:Facture')->findAll();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutService)];
        return new JsonResponse($response);


    }

    /**
     * @Route("devis/getDevis",name="get_devis_ajax")}
     */
    public function getDevisAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutService = $em->getRepository('AppBundle:Devis')->findAll();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutService)];
        return new JsonResponse($response);


    }

    /**
     * @Route("devis/getPaiement",name="get_paiement_admin_ajax")}
     */
    public function getPaimentAdminAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutService = $em->getRepository('AppBundle:Paiement')->findAll();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutService)];
        return new JsonResponse($response);


    }

    /**
     * @Route("devis/getDevis",name="get_paiement_client_ajax")}
     */
    public function getPaimentClientsAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutService = $em->getRepository('AppBundle:Paiement')->findAll();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutService)];
        return new JsonResponse($response);


    }

    /**
     * @Route("client/getClient",name="get_client_ajax")}
     */
    public function getClientsAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutClients = $em->getRepository('AppBundle:User')->retirerToutClient();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutClients)];
        return new JsonResponse($response);


    }

    /**
     * @Route("administrateur/getAdmin",name="get_administrateur_ajax")}
     */
    public function getAdministrateurAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutAdmin = $em->getRepository('AppBundle:User')->retirerToutAdminstrateur();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutAdmin)];
        return new JsonResponse($response);


    }

    /**
     * @Route("taxe/getTaxe",name="get_taxe_ajax")}
     */
    public function getTaxAjax()
    {
        $em = $this->getDoctrine()->getManager();
        $toutTaxe = $em->getRepository('AppBundle:Tax')->findAll();
        $response = ["data" => $this->get('gestion_client_service')->transformObjectToArray($toutTaxe)];

        return new JsonResponse($response);


    }

    /**
     * @Route("taxe/delete",name="taxe_delete",methods={"POST"})
     *
     */

    public function userDelete(Request $request)
    {


        $response = array('success' => false, 'message' => '', 'html' => '',);
        $repository = $this->getDoctrine()->getRepository('AppBundle:Tax');
        $id = $request->request->get('id');
        $taxe = $repository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($taxe);
        $em->flush();

        if (!$repository->find($id))
        {
            $response['success'] = true;
            $response['message'] = 'Success';
        }


        return new JsonResponse($request);

    }



}