<?php

namespace AppBundle\Controller;

use AppBundle\Form\LoginForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/",name="login_route")
     */
    public function loginAction()


    {
        $authUtils=$this->get('security.authentication_utils');

        $erreur=$authUtils->getLastAuthenticationError();
        $lastName=$authUtils->getLastUsername();
        $form=$this->createForm(LoginForm::class);
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('dashboard_index');
        }
        return $this->render('AppBundle:Security:login.html.twig',array( 'LoginForm' =>$form->createView() ,'erreur'=>$erreur));
    }

    /**
     * @Route("/logout",name="logout_route")
     */
    public function logoutAction()
    {
        throw new Exception('Erreur');
    }

    /**
     * @Route("/inscrire")
     */
    public function registreAction()
    {
        return $this->render('AppBundle:Security:registre.html.twig', array(
            // ...
        ));
    }

}
