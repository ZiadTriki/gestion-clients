<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\FactureForm;
use AppBundle\Form\ServiceForm;
use AppBundle\Form\TaxeForm;
use AppBundle\Form\UserForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 *
 */
class AdminController extends Controller
{


    /*********************************************************** *****************************************/

    /**
     * @Route("/dashbord",name="dashboard_index")
     */
    public function dashboardAction()
    {


        return $this->render('AppBundle:Admin:dashboard.html.twig');
    }

    /**
     *
     * @Route("/paiement",name="paiement_index")
     */
    public function paiementAction()
    {
        return $this->render('AppBundle:Admin/service:service.html.twig');
    }


    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/taxe",name="taxe_index")
     */
    public function taxeAction()
    {
        return $this->render('AppBundle:Admin/taxe:taxe.html.twig');
    }

    /**
     * @Route("/devis",name="devis_index")
     */
    public function devisAction()

    {
        return $this->render('AppBundle:Admin/service:service.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/service",name="service_index")
     */
    public function serviceAction()

    {
        return $this->render('AppBundle:Admin/service:service.html.twig',compact('toutService'));
    }

    /**
     * @Route("/facture",name="facture_index")
     */
    public function factureAction()
    {
        return $this->render('AppBundle:Admin/facture:facture.html.twig', array(
            // ...
        ));
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/clients",name="clients_index")
     */
    public function clientsAction()

    {
        return $this->render('AppBundle:Admin/user:clients.html.twig', array(
            // ...
        ));
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/admin",name="admin_index")
     */
    public function adminAction()
    {
        return $this->render('AppBundle:Admin/user:admin.html.twig', array(
            // ...
        ));
    }

    /*******************************************************************  ****************************************/

   /**
     * @Route("/service/ajouter",name="service_ajouter")
     */
    public function ajouterServiceAction(Request $request)

    {    $form=$this->createForm(ServiceForm::class);
         $form->handleRequest($request);

         if($form->isSubmitted() && $form->isValid())
         {

             $this->get('gestion_client_service')->inserFormToDataBase($form->getData());
             $this->addFlash('succes','Service est ajouer avec succes');
             return $this->redirectToRoute('service_ajouter');

         }
         return $this->render('AppBundle:Admin/service:ajouter_service.html.twig', array(
           'sericeForm'=> $form->createView()
        ));
    }

    /**
     * @Route("/facture/ajouter",name="ajouter_facture")
     */
    public function ajouterFactureAction(Request $request)

    {
        $forms=$this->createForm(FactureForm::class);
        $forms->handleRequest($request);
        if($forms->isSubmitted() && $forms->isValid())
        {
            $this->get('gestion_client_service')->inserFormToDataBase($forms->getData());
            $this->addFlash('succes','Facture Ajouté Avec Succes');
            return $this->redirectToRoute('ajouter_facture');
        }
        return $this->render('AppBundle:Admin/facture:ajouter_facture.html.twig', array(
             'formFacture'=> $forms->createView()
        ));
    }

    /**
     * @Route("utilsateur/ajouter",name="ajouter_utilisateur")
     */
    public function ajouterUtilisateurAction(Request $request)
    {
        $form=$this->createForm(UserForm::class);
        $form->handleRequest($request);
       if($form->isSubmitted()&& $form->isValid())
       {
           $this->get('gestion_client_service')->inserFormToDataBase($form->getData());
           $this->addFlash('succes','Utilisateur Est Ajouter Avec Succes');
           return $this->redirectToRoute('ajouter_utilisateur');
       }
        return $this->render('AppBundle:Admin/user:ajouter_utilisateur.html.twig', array(
           'formUser'=> $form->createView()
        ));
    }
    /**
     * @Route("Taxe/ajouter" ,name="ajouter_taxe")
     */

    public function ajouterTaxe(Request $request){
        $form=$this->createForm(TaxeForm::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $this->get('gestion_client_service')->inserFormToDataBase($form->getData());
            $this->addFlash('succes','Taxe est ajouer avec succes');
            return $this->redirectToRoute('ajouter_taxe');

        }
        return $this->render('AppBundle:Admin/taxe:ajouter_taxe.html.twig', array(
            'TaxeForm'=> $form->createView()
        ));
    }
    /******************************************************************************  ***************************************************/

}
