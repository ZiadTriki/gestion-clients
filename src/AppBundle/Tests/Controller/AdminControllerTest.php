<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase
{
    public function testDashboard()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/dashboard');
    }

    public function testService()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/service');
    }

    public function testFacture()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/facture');
    }

    public function testClients()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/clients');
    }

    public function testAdmin()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin');
    }

    public function testAjouterservice()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'service_ajouter');
    }

    public function testAjouterfacture()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'ajouter_facture');
    }

    public function testAjouterutilisateur()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'ajouter_utilisateur');
    }

}
